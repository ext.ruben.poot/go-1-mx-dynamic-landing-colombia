<?php
/**
 * Registration logic for the new ACF field type.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'init', 'go1mx_include_acf_field_home_landing' );
/**
 * Registers the ACF field type.
 */
function go1mx_include_acf_field_home_landing() {
	if ( ! function_exists( 'acf_register_field_type' ) ) {
		return;
	}

	require_once __DIR__ . '/class-go1mx-acf-field-home-landing.php';
	//require_once __DIR__ . '/inc/querys.php';

	acf_register_field_type( 'go1mx_acf_field_home_landing' );
}

function go1mx_landings(){
	$busqueda = $_GET["query"];
 //   echo "<h1>".$busqueda."</h1>";
	$args1 = array(
		'post_type'=>'dynamic-landing',
		'numberposts'=>'100',
		'orderby'=>'title',
		'order'=>'ASC',
		's'=>$busqueda
	);
	
	$args2 = array(
		'post_type'=>'page',
		'numberposts'=>'100',
		'orderby'=>'title',
		'order'=>'ASC',
		's'=>$busqueda
	);

	$query1 = get_posts( $args1 );

	$query2 = get_posts( $args2 );

	return array_merge($query1, $query2);
}

function go1mx_get_landings(){
	$busqueda = $_GET["query"];
	$args1 = array(
		'post_type'=>'dynamic-home',
		'numberposts'=>'100',
		'orderby'=>'title',
		'order'=>'ASC',
		's'=>$busqueda
	);
	
	$query1 = get_posts( $args1 );
	$datos = get_post_meta($busqueda, "contenido", true);
    $datos=str_replace("\'", "'",$datos);
    // echo $datos;
	return json_decode($datos);//$datos;
}

function go1mx_get_landings_header(){
	$busqueda = $_GET["query"];
	$args1 = array(
		'post_type'=>'dynamic-home',
		'post_per_page'=>'100',
		'orderby'=>'title',
		'order'=>'ASC',
		'p'=>$busqueda
	);
	
	$query1 = get_posts( $args1 );
$data=array(
    'backgroung'=>get_post_meta($busqueda, "banner_setings_backgroud_color_banner", true),
	'fontcolor'=>get_post_meta($busqueda, "banner_setings_font_color", true),
	'main_title'=>get_post_meta($busqueda, "banner_setings_title_header", true),
	'second_title'=>get_post_meta($busqueda, "banner_setings_sub_title", true)
);

	//$datos = get_post_meta($busqueda, "banner_setings_sub_title", true);
	return $data;//$datos;

}

function go1mx_get_landings_modifieddate(){
	$busqueda = $_GET["query"];
	$data_post=array(
		'post_modified'=>get_post_field('post_modified', $busqueda)
	);
	return $data_post;	

}

	
/** actualizar metepost */

function go1mx_get_landings_updatemeta(){
	$data=json_decode(file_get_contents('php://input'),true);
    $busqueda=$data['post_id'];    // post id

 if(json_last_error()===JSON_ERROR_NONE){	
	
    $dataupdate=json_encode($data['data'],JSON_UNESCAPED_UNICODE);    //valor nuevo
	$dataupdate=str_replace("'","\\\'",$dataupdate);    //valor nuevo
	$qtipo=$data['qtipo'];
	if($qtipo=='1'){
	$updatestatus  =update_metadata( 'post', $busqueda, 'contenido', $dataupdate);
	$datos = get_post_meta($busqueda, "contenido", true);
	return $datos;
	}
	if($qtipo=='0'){
		$datos = get_post_meta($busqueda, "contenido", true);
		return $datos;
	}
	}else{
	$datos = get_post_meta($busqueda, "contenido", true); //valor anterior
    //$datos=str_replace("\\\'","'",$datos);
	
	return $datos;
   }

}