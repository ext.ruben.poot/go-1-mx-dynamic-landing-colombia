<?php
namespace WPC;

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

class ShowLandingList extends \Elementor\Widget_Base
{
  public function __construct($data = [], $args = null) {
    parent::__construct($data, $args);
  }
  public function get_name()
  {
    return 'show-landing-list';
  }

  public function get_title()
  {
    return 'Show landing list';
  }

  public function get_icon() {
		return 'eicon-icon-box';
	}

  public function get_categories()
  {
    return ['basic'];
  }
  public function rangepublish($fecha_actual,$fecha_entrada,$fecha_salida){
  $publish=false;
  if($fecha_actual > $fecha_entrada && $fecha_actual<$fecha_salida)
  	{
  	  $publish=true;
  	}else
  		{
  		$publish=false;
  	}
  return $publish;
  }
  public function quitar_acentos($cadena){
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyyby';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    return utf8_encode($cadena);
}

protected function render() {
 global $post;
 $post_id =  $post->ID;
 //global $stitle;
 $site_url=get_site_url();
 $settingheader = wp_remote_get( $site_url.'/wp-json/go1mx/v1/header_home?query='.$post_id );
if( is_wp_error( $settingheader ) ) {
 return false;
 }
 $headerparams = wp_remote_retrieve_body( $settingheader);
 $settings_data= json_decode($headerparams,true);
 
 $bannercolor = $settings_data['backgroung'];
 $textos = $settings_data['fontcolor'];
 $stitle=$settings_data['second_title'];

 /* 
     'backgroung'=>get_post_meta($busqueda, "banner_setings_backgroud_color_banner", true),
	'fontcolor'=>get_post_meta($busqueda, "banner_setings_font_color", true),
	'main_title'=>get_post_meta($busqueda, "banner_setings_title_header", true),
	'second_title'=>get_post_meta($busqueda, "banner_setings_sub_title", true)

 */
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<style media="screen">
.contenedor-bloques{
width:100%;
margin:10px 15px  ;
display: flex;
gap:20px;   /* hoy */
flex-wrap: wrap;

}
.contenedor-bloques li{
width:33%;
height:200px; /*hoy */
display:flex;
flex-direction:column;
}


   .card-title-promo {
       font-size: 17px;
       margin-bottom: 5px;
       font-weight: bold;
       overflow: hidden;
       text-overflow: ellipsis;
       width: 100%;
       white-space: nowrap;
       color: #333;
   }

   /*.card-subtitle-promo {
       overflow: hidden;
       position: relative;
       max-height: 6.5em;   
       padding-right: 1em;
       color: #333;
       font-size:17px;
     }*/
  .card-subtitle-promo {
    overflow: hidden;
    position: relative;
    height: 4.5em;
    min-height: 3.5em;
   
    padding-right: 1em;
    color: #333;
    font-family: Arial,sans-serif;
    font-size: 13px;  
  }
     .cardImage .cardImage-img {
         width: 100%;
         display: block;
         max-width: 100%;
         height: auto;
     }

     .responsive-banner {
         display: inline-block;
         margin: 0px;
         width: 100%;
         position: relative;
         overflow: hidden;
         background-image: linear-gradient(to bottom right, <?php echo $settings_data['backgroung'] ?>, <?php echo $settings_data['backgroung'] ?>);
         background-repeat: no-repeat;
         text-align: left;
         box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
       }
       .container-envelope {
         max-width: calc(100%);
         padding: 5px;
         color: #fff;
          display: -webkit-box;
           display: -webkit-flex;
           display: -ms-flexbox;
           display: flex;
         min-height: 200px;
         max-height: 300px;
       }
       svg, #banner1 {
         fill: rgba(0, 0, 0, 0.1);
         position: absolute;
       }
       #banner1 {
         max-width : 100%;
       }
       #banner1 {
         top: 0px;
         right: 0;
       }

       <?php if(!empty($bannercolor)): ?>
        .wmk-bc-header{
                background: <?php echo $bannercolor; ?> !important;
        }
       <?php endif; ?>
       
       <?php if(!empty($textos)): ?>
       .wmk-bc-header .wmk-bc-img h4{
             color: <?php echo $textos; ?>;
       }
       .wmk-bc-header .wmk-bc-img h6{
             color: <?php echo $textos; ?>;
       }
       .wmk-bc-header .wmk-bc-img h5{
             color: <?php echo $textos; ?>;
             width: 500px;
             
       }

       <?php endif; ?>

       


       <?php if(!empty($textos)): ?>
            .wmk-item-info .wmk-ii-2 .wmk-truncate span+span{
            color: <?php echo $textos; ?> !important;
            }
            .wmk-item-info .wmk-ii-1 span+span+span{
            color: <?php echo $textos; ?> !important;
            }
            .wmk-item-info .wmk-ii-1 span{
            color: <?php echo $textos; ?> !important;
            }
            .wmk-item-info .wmk-ii-1{
            color: <?php echo $textos; ?> !important;
            }
            .wmk-item-info .wmk-ii-2 span{
            color: <?php echo $textos; ?> !important;
            }
    <?php endif; ?>
    /*.active{
      background: rgb(255, 189, 51);
    }*/
    
  .nav-item a {
  color:#0693e3;
  background-color:#8ed1fc3b;
  text-decoration:none;
  font-weight: bold;
  font-size:18px;
  width:200px;
  height:50px;
  text-align:center;
  padding: 10px 10px;
  margin: 5px 5px 5px 2px;
 }
    .nav-link.active, .show>.nav-pills .nav-link {
    background: #699dcd !important;
    color: #fff;
    font-weight: bold;
    text-align:center;
    padding: 10px;
    /*border-radius: 10px 10px 10px 10px;*/
    /*transform:skewX(35deg);*/

}

btn-success {
    color: #2E318C;
    background-color: #FFDB00;
    border-color: #e69b00;
    
  
    
}
.btn {
    display: inline-block;
    font-weight: 400;
    color: #2E318C;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: #FFDB00;
    border: 2px solid transparent;
    padding: 10px 25px;
    font-size: 14px;
    line-height: 1.5;
    border-radius: 0.25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.card{
  border-radius: 0.75rem;
}
.card2 {
overflow: hidden;
padding: 0px 0px;
margin: 3px;
border-radius: 0.25rem;
  &:hover {
    transition: all 0.2s ease-out;
    box-shadow: 0 15px 15px rgba(38,38,38,0.12), 0 15px 15px rgba(38,38,38,0.12);
    border-radius: 0.75rem;
    top: 0px;
    border: 0px solid #cccccc;
    background-color: white;
    text-decoration: none;
  }
  &:hover:before {
    border-radius: 0.75rem;
    transform: scale(2);
    transform-origin: 50% 50%;
    transition: transform 0.15s ease-out;

  }
}
.card2 div:hover img {
  transform: scale(0.99);
}
</style>

<?php
//iniciamos el codigo para pintar los tab
$request = wp_remote_get( $site_url.'/wp-json/go1mx/v1/list_landings?query='.$post_id);
if( is_wp_error( $request ) ) {
 return false;
 }
$body = wp_remote_retrieve_body( $request);
$data = json_decode($body,true);
//echo $data;
?>
<div class="container mt-3">
<ul class="nav-pills nav justify-content-center" id="myTab" role="tablist">
<?php

if(empty($data)){

} else{
  $numseccion=0;
foreach ($data as $value1) {
  $nombreid=strtolower($value1['title']);
  $nombreid=str_replace(' ','',$nombreid);
  $nombreid=$this->quitar_acentos($nombreid);

?>
<li class="nav-item">
  <a  class="nav-link " id="<?php echo $nombreid ?>" name="<?php echo $nombreid ?>"  data-toggle="tab" href="#<?php echo "seccion".$numseccion?>" role="tab" aria-controls="<?php echo "seccion".$numseccion ?>" aria-selected="true"><?php echo $value1['title']; ?></a>
</li>
<?php
$numseccion++;
}
?>
</ul>
<div class="tab-content" id="mycontenedor">
  <?php
    $numseccion=0;
    foreach ($data as $value1) {
  ?>
  <div class="tab-pane" id="<?php echo "seccion".$numseccion?>" role="tabpanel" aria-labelledby="home-tab">
   <div class="contenedor-bloques">
     <?php
     $contador=count($value1['pages']);
     for ($i = 0; $i < $contador; ++$i){
       $rfechas=explode("-",$value1['pages'][$i]['dates'][0]);
       $fecha_actual = strtotime(date("d-m-Y H:i:00",time()))-14200;
       $xfecha_inicio=trim($rfechas[0]);
       $xfecha_final =trim($rfechas[1]);
       $fecha_inicio = strtotime(str_replace("/","-",$xfecha_inicio). " 01:00:00");
       $fecha_final  = strtotime(str_replace("/","-",$xfecha_final)." 23:59:00");
       if(($fecha_actual > $fecha_inicio) && ($fecha_actual < $fecha_final)){
       ?>
      <a href="<?php echo $value1['pages'][$i]['link'] ?>" class="card2">

       <div class="card gap-2 mt-3" style="width: 21rem;">
         <img class="cardImage-img" style="border-radius: 0.75rem;
" src="<?php echo $value1['pages'][$i]['image']?>"  alt="...">
             <div class="card-body" style="background-color: #FcFcFc; color:#333">
                <h6 class="card-title-promo"><?php echo $value1['pages'][$i]['title'] ?></h6>
                 <p class="card-subtitle-promo"><?php echo $value1['pages'][$i]['description'] ?></p>
         
            </div>
            <!--
            <div style="padding: 0px 15px 15px 15px;" class="ml-1">
                <a href="<?php //echo $value1['pages'][$i]['link'] ?>" class="btn btn-warning"><b>Ver ofertas</b></a>
            </div>-->
       </div>
       </a>
      
 <?php
       }else{ 
              //echo $fecha_final ."   title ". $value1['pages'][$i]['title']."<br>";   
              if(($fecha_actual > $fecha_inicio) && ($fecha_final=='-62169897660')){

              ?>
      <a href="<?php echo $value1['pages'][$i]['link'] ?>" class="card2">
              
              <div class="card gap-2 mt-3" style="width: 21rem;">
                  <img class="cardImage-img" style="border-radius: 0.75rem;
" src="<?php echo $value1['pages'][$i]['image']?>"  alt="...">
                  <div class="card-body" style="background-color: #FcFcFc; color:#333">
                        <h6 class="card-title-promo"><?php echo $value1['pages'][$i]['title'] ?></h6>
                        <p class="card-subtitle-promo"><?php echo $value1['pages'][$i]['description'] ?></p>
         
                 </div>
                 <!--
                  <div style="padding: 0px 15px 15px 15px;" class="ml-1">
                      <a href="<?php //echo $value1['pages'][$i]['link'] ?>" class="btn btn-warning"><b>Ver ofertas</b></a>
                  </div> -->
                </div>
              
              </a>      
          
          <?php  
            }

       }

     }
 ?>
</div>
</div>
<?php
$numseccion++;
}

 ?>
</div>
<script>
  //var xelementes=0;
/*  $(function () {
    $('#myTab li:first-child a').tab('show')
    xelementes=$(".nav-item").length
  })*/
  
  var booker = Booker("place_name_hotel");
  //alert(window.location.href);

  $(document).ready(function(){   
  //cad = location.toString();
  cad= window.location.href;
  x=cad.indexOf("#") > -1;
      
    if(x==true){
            var ancla = cad.split('#')[1];

            $('#myTab li a').removeClass('active')
            var indextab=$('#myTab li a').index($('#'+ ancla))
            $($('#myTab li a')[indextab]).addClass('active')
            
            var anclaseccion = $('#myTab li a')[indextab]['href'].split('#')[1];
            var indexseccion=$($('#'+anclaseccion)).index()

            $(".tab-pane").removeClass('active show')  //elimino la clases
            $('#'+anclaseccion).addClass('active show')     //asigno las clases
    }else{

      $(function () {
    $('#myTab li:first-child a').tab('show')
    xelementes=$(".nav-item").length
  })


    }
    
   
    }) 
// Selecciona todos los elementos de nav-link
document.querySelectorAll('.nav-link').forEach(function(navLink) {
    // Agrega el evento click a cada nav-link
    navLink.addEventListener('click', function() {
        var sectionName = this.id;  // Obtiene el id del nav-link

        // Envía los datos al dataLayer
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'event': 'gtmEvent',
            'eventCategory': 'ofertas seccion',
            'eventAction': window.location.pathname,  // Obtiene dinámicamente el path de la URL
            'eventLabel': sectionName  // Usa el id del nav-link como nombre de la sección
        });

        console.log('Evento enviado: ', sectionName);  // Para depurar
    });
});
   

</script>
</div>
<?php
}
}//final del render
}//fin de la clase ShowLandingList
?>
