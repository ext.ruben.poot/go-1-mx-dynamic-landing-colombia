<?php

namespace WPC;

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}


class HotelesDinamicos extends \Elementor\Widget_Base
{

    
  public function __construct($data = [], $args = null) {
    parent::__construct($data, $args);
   
  }

  public function get_name()
  {
    return 'hoteles-dinamicos';
  }

  public function get_title()
  {
    return 'Hoteles dinamicos';
  }
 
  public function get_icon() {
		return 'eicon-icon-box';
	}

  public function get_categories()
  {
    return ['basic'];
  }

  protected function render() {
    global $post;
    $desde = get_post_meta($post->ID, "vigencia_desde", true);
    $hasta = get_post_meta($post->ID, "vigencia_hasta", true);
    $now = strtotime("-5 hours");
    $bannercolor = get_post_meta($post->ID, "colores_banner", true);
    $textos = get_post_meta($post->ID, "colores_textos", true);
    $bgPrecio = get_post_meta($post->ID, "colores_bg_precio", true);
    $bgDestino = get_post_meta($post->ID, "colores_bg_destino", true);
    $pleca_color = get_post_meta($post->ID, "colores_pleca_hotel", true);
    if((empty($desde) || strtotime($desde) < $now) &&  (empty($hasta) || strtotime($hasta) > $now)):
  
    
  ?>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-v4-grid-only@1.0.0/dist/bootstrap-grid.css">
  <style>

#hoteles_container.cargando{
        height: 300px;
        position: relative;
    }
    #hoteles_container.cargando::before{
        position: absolute;
        width: 100%;
        left: 0;
        top: 0;
        height: 100%;
        content: '';
        z-index: 1;
        background-color: #fff;
        
    }
    #hoteles_container.cargando::after{
        position: absolute;
        width: 100%;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        height: 80px;
        width: 80px;
        content: '';
        z-index: 2;
        background-image: url('https://static.cdnpth.com/img/loader-logo.svg');
        background-size: 100% 100%;
        background-position: center center;
        background-repeat: no-repeat;
        animation: linear 2s d infinite;
    }
    
    @keyframes d{
        0%{transform:rotate(0) scale(1);animation-timing-function:cubic-bezier(.55,.055,.675,.19)}
        50%{transform:rotate(400deg) scale(.6);animation-timing-function:cubic-bezier(.215,.61,.355,1)}to{transform:rotate(3turn) scale(1)}}

.item-individual .wmk-stars-hotel{
    top: 17px;
}

#hoteles_container{
    margin:0;
}

#hoteles_container > div{
    margin-bottom:32px;
}
.vmk-ribbon{
    background-color: #ff0172;
}

@media (min-width:768px) and (max-width:1024px),(min-width:1025px){.owl-theme.c-carousel-2 .owl-dots{display:none!important}}.wmk-item-carousel-2{border-radius:10px 10px 0 0;overflow:hidden}.wmk-item-carousel-2 .vmk-ribbon{box-shadow:0 .5rem 1rem rgba(0,0,0,.15);border-radius:50px;color:#fff;font-size:18px;line-height:1;margin-left:20%;padding:0 40px 0 15px;height:38px;display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;font-family:Roboto;font-size:16px;font-weight:400}.wmk-item-carousel-2 .vmk-ribbon .vmk-text-lg{font-size:20px;font-weight:600}@media (max-width:767px){.wmk-item-carousel-2 .vmk-ribbon{margin-left:25%}}@media (min-width:768px) and (max-width:1024px){.wmk-item-carousel-2 .vmk-ribbon{margin-left:0}}@media (min-width:1025px){.wmk-item-carousel-2 .vmk-ribbon{margin-left:28%}}.wmk-item-carousel-2 .vmk-ribbon .vmk-text-shadow span+span{position:relative;top:-2px}.wmk-item-carousel-2 .vmk-ribbon img{background-color:#fff;border-radius:50px;bottom:0;height:38px;margin:auto;padding:8px;position:absolute;right:-1px;top:-1px;width:38px!important}.wmk-item-carousel-2 img{border-radius:10px 10px 0 0;position:absolute;z-index:0;width:100%!important}.wmk-item-carousel-2 .wmk-container{border-radius:10px;display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;-ms-flex-wrap:wrap-reverse;flex-wrap:wrap-reverse;height:180px;-ms-flex-pack:justify;justify-content:space-between;-ms-flex-align:stretch;align-items:stretch;-ms-flex-line-pack:stretch;align-content:stretch;position:relative}.wmk-item-carousel-2 .wmk-container .wmk-c-top{color:#fff;margin:15px;position:relative;text-shadow:0 2px 2px rgba(0,0,0,.25)!important}.wmk-item-carousel-2 .wmk-container .wmk-c-bottom{color:#fff;margin:15px 15px 0;padding:4px 0;position:relative;text-shadow:0 2px 2px rgba(0,0,0,.25)!important}.wmk-item-carousel-2 .wmk-container .wmk-c-bottom:before{content:"";background:rgba(46,6,117,.55);bottom:0;left:-15px;position:absolute;right:-15px;top:0}.wmk-item-carousel-2 .wmk-container .wmk-c-bottom .wmk-cb-destino{display:block;font-size:16px;font-weight:700;text-align:left;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;max-width:100%}.wmk-item-carousel-2 .wmk-container .wmk-c-bottom .wmk-cb-hotel-name{font-size:16px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;max-width:90%}.wmk-item-carousel-2 .wmk-container .wmk-c-bottom .b-col-6{padding-left:10px;padding-right:10px}.wmk-item-carousel-2 .wmk-stars-hotel{position:absolute}.wmk-item-carousel-2 .wmk-stars-hotel .wmk-star{float:left;width:10px}.wmk-item-carousel-2 .wmk-stars-hotel .wmk-star img{height:10px;position:relative;width:10px}@media (min-width:1025px){.item-ml-10{margin-left:-10px}}.wmk-item-info{background-color:#fff;border:1px solid #e5e5e5;border-radius:0 0 5px 5px;overflow:hidden;padding:0 15px}.wmk-item-info img{position:relative;top:4px}.wmk-item-info .wmk-ii-1{background-color:rgba(81,64,142,.2);color:#51408e;padding:10px 10px 5px;text-align:right}.wmk-item-info .wmk-ii-1 .line-middle{-webkit-text-decoration-line:line-through;text-decoration-line:line-through}.wmk-item-info .wmk-ii-1 span{color:#51408e;display:block;line-height:1.4}@media (min-width:768px) and (max-width:1024px){.wmk-item-info .wmk-ii-1 span{display:inline-block}}.wmk-item-info .wmk-ii-1 span+span{color:#6a707c}@media (min-width:768px) and (max-width:1024px){.wmk-item-info .wmk-ii-1 span+span{display:inline-block}}.wmk-item-info .wmk-ii-1 span+span+span{color:#51408e;font-size:18px;font-weight:600}@media (min-width:768px) and (max-width:1024px){.wmk-item-info .wmk-ii-1 span+span+span{display:block}}.wmk-item-info .wmk-ii-2{padding:10px 5px}.wmk-item-info .wmk-ii-2 .wmk-c-people span{margin-right:5px}.wmk-item-info .wmk-ii-2 .wmk-icon{left:2px;position:absolute}.wmk-item-info .wmk-ii-2 .b-row{margin:auto!important}.wmk-item-info .wmk-ii-2 span{color:#51408e;font-size:14px}.wmk-item-info .wmk-ii-2 .wmk-truncate span{color:#6a707c}.wmk-item-info .wmk-ii-2 .wmk-truncate span+span{color:#51408e}.wmk-item-info .wmk-ii-2 .s-img{margin-left:15px}.wmk-item-info .wmk-ii-2 hr{border-color:#51408e;margin:5px -15px}.wmk-item-info .wmk-ii-2 .f-semibold{font-weight:600}.p-relative{position:relative}.wmk-truncate{padding-left:21px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;max-width:100%}.item-individual .wmk-stars-hotel{top:17px}



<?php if(!empty($bannercolor)): ?>
.wmk-bc-header{
    background: <?php echo $bannercolor; ?> !important;
}
<?php endif; ?>

<?php if(!empty($pleca_color)): ?>
.vmk-ribbon{
    background-color: <?php echo $pleca_color; ?> !important;
}
<?php endif; ?>


<?php if(!empty($textos)): ?>
       .wmk-bc-header .wmk-bc-img h1{
             color: <?php echo $textos; ?>;
       }
       .wmk-bc-header .wmk-bc-img h6{
             color: <?php echo $textos; ?>;
       }

  <?php endif; ?>


<?php if(!empty($textos)): ?>
.wmk-item-info .wmk-ii-2 .wmk-truncate span+span{
    color: <?php echo $textos; ?> !important;
}
.wmk-item-info .wmk-ii-1 span+span+span{
    color: <?php echo $textos; ?> !important;
}
.wmk-item-info .wmk-ii-1 span{
    color: <?php echo $textos; ?> !important;
}
.wmk-item-info .wmk-ii-1{
    color: <?php echo $textos; ?> !important;
}
.wmk-item-info .wmk-ii-2 span{
    color: <?php echo $textos; ?> !important;
}
<?php endif; ?>


<?php if(!empty($bgDestino)): ?>
.wmk-item-carousel-2 .wmk-container .wmk-c-bottom:before{
    background: <?php echo $bgDestino; ?> !important;
}
<?php endif; ?>

<?php if(!empty($bgPrecio)): ?>
.wmk-item-info .wmk-ii-1{
    background-color: <?php echo $bgPrecio; ?> !important;
}
<?php endif; ?>

</style>
  </style>
  <div id="hoteles_container" class="row cargando">
   
  </div>
    <div style="display:none">
    <div id="hotel_template">
			<a class="item item-individual link_url" href="" target="_blank">
        <!-- carousel 1 -->
        <div class="wmk-item-carousel-2"> 
            <div class="wmk-container">
            <img class="img-bg lazy-loaded">   
            <div class="wmk-c-top">
                <div class="vmk-ribbon">
                    <span class="vmk-text-shadow">
                        <span class="vmk-text-lg vmk-discount"></span>
                        <span>de descuento</span>
                    </span>
                    <span>
                        <img class="lazy-loaded" src="https://www.pricetravel.com/ofertas-de-viajes/wp-content/uploads/2022/08/hotel.svg" data-lazy-type="image" data-src="https://www.pricetravel.com/ofertas-de-viajes/wp-content/uploads/2022/08/hotel.svg"><noscript><img src="https://www.pricetravel.com/ofertas-de-viajes/wp-content/uploads/2022/08/hotel.svg"></noscript>
                    </span>
                </div>    
            </div>
            <div class="wmk-c-bottom">
                <div class="b-row">
                    <div class="b-col-12">
                        <div class="wmk-cb-hotel-name">Dreams Los Cabos Suites Golf Resort and Spa</div>
                        <div class="wmk-stars-hotel b-d-none">
                        </div>
                    </div>
                    <div class="b-col-12">
                        <span class="wmk-cb-destino">Los Cabos</span>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <div class="wmk-item-info">
        <div class="b-row">
            <div class="b-col-4 b-col-md-12 b-col-lg-4 wmk-ii-1">
                <span>Desde</span>
                <span class="line-middle wmk-item-info-precio">_</span>
                <span class="wmk-item-info-promo"></span>
                <small style="font-size: 12px;line-height: 14px;display: block;">Impuestos no incluidos</small>
            </div>
            <div class="b-col-8 b-col-md-12 b-col-lg-8 b wmk-ii-2">
                <div class="b-row p-relative">
                    <span class="wmk-icon">
                        <img class="lazy-loaded" src="https://www.pricetravel.com/ofertas-de-viajes/wp-content/uploads/2022/08/hotel_black.svg" data-lazy-type="image" data-src="https://www.pricetravel.com/ofertas-de-viajes/wp-content/uploads/2022/08/hotel_black.svg"><noscript><img src="https://www.pricetravel.com/ofertas-de-viajes/wp-content/uploads/2022/08/hotel_black.svg"/></noscript>
                    </span>
                    <span class="wmk-truncate">
                       <span>Desde</span>
                       <span class="f-semibold fecha-inicio"></span>
                    </span>
                </div>
                <div class="b-row p-relative">
                    <span class="wmk-truncate">
                       <span>Hasta</span>
                       <span class="f-semibold fecha-fin"></span>
                    </span>
                </div>
                <div class="b-col-12">
                    <hr>
                </div>
                <div class="b-row wmk-c-people">
                    <span>
                        <img class="lazy-loaded" src="https://www.pricetravel.com/ofertas-de-viajes/wp-content/uploads/2022/08/people_black.svg" data-lazy-type="image" data-src="https://www.pricetravel.com/ofertas-de-viajes/wp-content/uploads/2022/08/people_black.svg"><noscript><img src="https://www.pricetravel.com/ofertas-de-viajes/wp-content/uploads/2022/08/people_black.svg"/></noscript>
                    </span>
                    <span class="f-semibold">2 personas</span>
                </div>
            </div>
        </div>
    </div>  

</a>   
				</div>
</div>
<div id="hoteles_container_script"></div>
<script>
  window.site_url = '<?php echo get_site_url(); ?>';
  window.post_id = '<?php echo $post->ID; ?>';
  window.campaignToken = "<?php echo $_GET['CampaignToken'] ?>";
  
</script>
<script src="<?php echo plugins_url( '/hoteles_dinamicos.js?v=32', __FILE__ ) ?>"></script>
  <?php
    endif;
  }

}